from django import forms


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=100)
    first_name = forms.CharField(max_length=100)
    last_name = forms.CharField(max_length=100)
    password = forms.CharField(
        max_length=100,
        widget=forms.PasswordInput,
    )
    password_confirmation = forms.CharField(
        max_length=100,
        widget=forms.PasswordInput,
    )


class SignInForm(forms.Form):
    username = forms.CharField(max_length=100)
    password = forms.CharField(
        max_length=100,
        widget=forms.PasswordInput,
    )
