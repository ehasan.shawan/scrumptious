from django.db import models

# Create your models here.


class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField(null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class RecipeStep(models.Model):
    step_number = models.PositiveSmallIntegerField()
    instruction = models.TextField()
    recipe = models.ForeignKey(
        Recipe, related_name="steps", on_delete=models.CASCADE,)

    class META:
        ordering = ["step_number"]


class Ingredient (models.Model):
    amount = models.CharField(max_length=100)
    food_item = models.CharField(max_length=100)
    recipe = models.ForeignKey(
        Recipe, related_name="ingredients", on_delete=models.CASCADE, )

    class META:
        ordering = ["food_item"]
